class Stack:
    def __init__(self):
        self.stack = []

    def push(self, obj):
        self.stack.append(obj)

    def pop(self):
        if len(self.stack):
            return self.stack.pop()
        else:
            raise Exception('stack underflow error')

    def __len__(self):
        return len(self.stack)


s = Stack()
s.push(1)
s.push(2)
s.push(3)

"""the brace matching problem"""

brace_stack = Stack()

open_braces = '[{('
close_braces = ']})'

matching_braces = {'}': '{', ']': '[', ')': '('}


def check_braces(string):
    for index, char in enumerate(string):
        if char in open_braces:
            brace_stack.push(char)
        elif char in close_braces:
            if len(brace_stack) and brace_stack.pop() == matching_braces[char]:
                pass
            else:
                print("error matching {0} at index {1}".format(char, index))
                break

    if len(brace_stack):
        print("brace character {0} not closed".format(brace_stack.pop()))


s1 = "(1 + 2})"
check_braces(s1)