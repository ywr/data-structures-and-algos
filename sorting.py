class lists(object):

    """Bubble sort, selection sort and insertion sort are O(n^2) algorithms"""

    def __init__(self, alist):
        self.alist = alist

    def bubblesort(self):
        a = self.alist
        n = len(a)
        for i in range(n-1):
            for flag in range(n-1-i):
                if a[flag] > a[flag+1]:
                    a[flag], a[flag+1] = a[flag+1], a[flag]
        print(a)

    def selection_sort(self):
        a = self.alist
        n = len(a)
        for i in range(n-1):
            current = i
            for flag in range(i,n):
                if a[flag] <= a[current]:
                    current = flag
            a[i], a[current] = a[current], a[i]
        print(a)

    def insertion_sort(self):
        a = self.alist
        n = len(a)
        index = 0
        for i in range(1, n):
            clone = a[i]
            for flag in range(i, -1, -1):
                if clone < a[flag-1]:
                    print(i, a[flag])
                    a[flag] = a[flag-1]
                else:
                    index = flag
                    break
            a[index] = clone
        print(a)

    """Merge sort is an O(n) algorithm"""

    def merge_sort(self):
        n = self.alist

        def merge(n):
            merged = []
            if len(n) < 2:
                return n
            mid = len(n)//2

            # Recur before n becomes a list of single element
            a = merge(n[:mid])
            print(a)
            b = merge(n[mid:])
            print(b)

            while len(a) > 0 or len(b) > 0:
                try:
                    if a[0] <= b[0]:
                        merged.append(a[0])
                        a.pop(0)
                    else:
                        merged.append(b[0])
                        b.pop(0)
                except IndexError:
                    if len(a) > 0:
                        merged.extend(a)
                        a = []
                    if len(b) > 0:
                        merged.extend(b)
                        b = []
                    else:
                        pass
            return merged

        print(merge(n))

    def quick_sort(self):
        n = self.alist

        def sort(n):
            print("sorting %s" % n)
            less = []
            equal = []
            greater = []

            if len(n) > 1:
                pivot = n[0]
                print("pivot:%s" % pivot)
                for x in n:
                    if x < pivot:
                        less.append(x)
                        print("less:%s" % less)
                    if x == pivot:
                        equal.append(x)
                        print("equal:%s" % equal)
                    if x > pivot:
                        greater.append(x)
                        print("greater:%s" % greater)
                print("sorted around the pivot", less, equal, greater)
                return sort(less) + equal + sort(greater)
            else:
                print("straight returned", n)
                return n

        print(sort(n))


testlist = [3, 1, 4, 1, 5, 9, 2, 6]
testlist = lists(testlist)

testlist.insertion_sort()

