class Node(object):
    def __init__(self, left=None, right=None, data=None):
        self.left = left
        self.right = right
        self.data = data

    def __repr__(self):
        return 'Node(left = {0}, right = {1}, data = {2})'.format(repr(self.left), repr(self.right), repr(self.data))


class BST(object):
    def __init__(self, nodes=None):
        if nodes:
            self.root = nodes
        else:
            self.root = Node()

    def __repr__(self):
        return 'BST(nodes={0})'.format(repr(self.root))

    def add_node(self, branch, new_node):
        if new_node.data < branch.data:
            if branch.left is None:
                branch.left = new_node
                new_node.parent = branch
            else:
                self.add_node(branch.left, new_node)
        else:
            if branch.right is None:
                branch.right = new_node
                new_node.parent = branch
            else:
                self.add_node(branch.right, new_node)

    def add(self, data):
        if self.root.data == None:
            self.root.data = data

        else:
            self.add_node(self.root, Node(data=data))

    def find(self, data):
        if self.root == None:
            return None
        return self.find_node(self.root, data)

    def find_node(self, branch, data):
        if branch.data == data:
            return branch

        if data < branch.data:
            if branch.left is None:
                return None
            else:
                return self.find_node(branch.left, data)
        else:
            if branch.right is None:
                return None
            else:
                return self.find_node(branch.right, data)

    def delete(self, value):
        self.delete_node(self.find(value))

    def delete_node(self, node):

        if node == None:
            return None

        children = 0
        if node.left:
            children += 1
        if node.right:
            children += 1

        # Leaf node case
        if children == 0:
            if node.parent != None:
                if node.parent.left is node:
                    node.parent.left = None
                else:
                    node.parent.right = None

        if children == 1:
            child = node.left if node.left != None else node.right
            if node == self.root:
                self.root = child
                self.root.parent = None
            elif node.parent.left == node:
                node.parent.left = child
            else:
                node.parent.right = child

        if children == 2:
            # find node on the right branch with the smallest value
            smallest = node.right
            while smallest.left != None:
                smallest = smallest.left
            node.data = smallest.data
            self.delete_node(smallest)

    def visualize(self, branch=None, depth=0):
        branch = branch or self.root

        left = right = ''

        if branch.left:
            left = self.visualize(branch.left, depth + 1)

        if branch.right:
            right = self.visualize(branch.right, depth + 1)

        return left + "{0}{1}\n".format('-' * (depth + 1), str(branch.data)) + right

    def as_list(self, branch=None):
        branch = branch or self.root

        left = right = []

        if branch.left:
            left = self.as_list(branch.left)

        if branch.right:
            right = self.as_list(branch.right)

        return left + [branch.data] + right
