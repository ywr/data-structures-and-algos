empty_rlist = None


def make_rlist(first, rest = empty_rlist):
    return first, rest


def first(l):
    return l[0]


def rest(l):
    return l[1]


def len_rlist(l):
    length = 0
    while l != 0:
        l, length = rest(l), length + 1
    return length


"""Recursively appending an element (y) to a list (l)"""


def append(l, y):
    if l == empty_rlist:
        return make_rlist(y, empty_rlist)
    return make_rlist(first(l), append(rest(l), y))
# >>> l = (1, (2, (3, None)))
# >>> append(l, 4)
# Out = (1, (2, (3, (4, None))))


def prepend(l, y):
    return make_rlist(y, l)


def map_linked(p, l):
    if l == empty_rlist:
        return l
    return make_rlist(p(first(l)), map_linked(p, l))


def filter_linked(pred, l):
    if rest(l) == empty_rlist:
        return l if pred(first(l)) else None
    elif pred(first(l)):
        return make_rlist(first(l), filter_linked(pred, rest(l)))
    else:
        return filter_linked(pred, rest(l))
# pred = lambda x: (x < 0)
# print(filter_linked(pred, (1, (-2, (3, (-4, (5, None)))))))
# (-2, (-4, None))


def reduce_linked(p, l):
    if rest(l) == empty_rlist:
        return first(l)
    else:
        return p(first(l), reduce_linked(p, rest(l)))


def rlist_from_tuple(tup):
    if len(tup) == 0:
        return empty_rlist
    else:
        return tup[0], rlist_from_tuple(tup[1:])